package com.isxaker;

public class Main {

    public static void main(String[] args) {
        int[] arr1 = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] arr2 = {3, 2, 1, 0};
        int[] arr3 = {2, 1, 0};
        int[] arr4 = {1};

        BinaryHeap heap = new BinaryHeap(arr1);
        arr1 = heap.heapSort();
        arr2 = heap.heapSort(arr2);
        arr3 = heap.heapSort(arr3);
        arr4 = heap.heapSort(arr4);
    }
}
