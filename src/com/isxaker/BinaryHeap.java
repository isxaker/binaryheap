package com.isxaker;

/**
 * Created by mikhail on 30.01.2016.
 */
public class BinaryHeap {
    private int[] a;
    private int size;

    public BinaryHeap() {
        this.size = 0;
        this.a = new int[10];
    }

    public BinaryHeap(int[] a) {
        buildHeap(a);
    }

    private void buildHeap(int[] a) {
        this.size = a.length;
        this.a = a.clone();
        for (int i = a.length / 2 - 1; i >= 0; i--) {
            this.hepify(i);
        }
    }

    public int[] heapSort() {
        for (int i = this.size - 1; i > 0; i--) {
            swap(this.a, i, 0);
            this.size--;
            this.hepify(0);
        }

        return this.a;
    }

    public int[] heapSort(int[] arr) {
        this.buildHeap(arr);
        return heapSort();
    }

    public int getSize() {
        return size;
    }

    private void hepify(int i) {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int largest = i;

        if (left < this.size && this.a[largest] < this.a[left]) {
            largest = left;
        }

        if (right < this.size && this.a[largest] < this.a[right]) {
            largest = right;
        }

        if (i != largest) {
            swap(a, i, largest);
            hepify(largest);
        }
    }

    public void add(int newElem) {
        if (this.a.length < this.size + 1) {
            resize();
        }
        //adding
        this.a[size] = newElem;
        this.size++;
        swim(size - 1);
    }

    private void swim(int index) {
        while (index > 0 && this.a[index] > this.a[index / 2]) {
            swap(this.a, index, index / 2);
            index /= 2;
        }
    }

    private void swap(int[] arr, int one, int two) {
        int temp = arr[one];
        arr[one] = arr[two];
        arr[two] = temp;
    }

    private void resize() {
        int[] newArr = new int[this.size * 2 / 3];
        System.arraycopy(this.a, 0, newArr, 0, this.a.length);
        this.a = newArr;
    }
}
